---
title: Support Portal
description: Support Portal
support-hero:
  data:
    title: Support Portal
side_menu:
  anchors:
    text: 'ON THIS PAGE'
    data:
      - text: 'First time reaching support?'
        href: '#first-time-reaching-support'
        nodes:
          - text: 'Setting up an account on support.gitlab.com'
            href: '#setting-up-an-account-on-supportgitlabcom'                                  
      - text: "Having trouble with the Support Portal?"
        href: "#having-trouble-with-the-support-portal"
        nodes:
          - text: "Language Support"
            href: "#language-support"                                       
  hyperlinks:
    text: ''
    data: []
components:
- name: support-copy-markdown
  data:
    block:
    - header: First time reaching support?
      id: first-time-reaching-support
      text: |
        If you just purchased a license, there's a few things to do to make sure the folks maintaining your GitLab get the smoothest support experience possible.
        
        1. [Set up an account](#setting-up-an-account-on-supportgitlabcom) on [support.gitlab.com](https://support.gitlab.com)

        2. Create your first ticket [to register your company's authorized support contacts](/support/managing-support-contacts/#getting-set-up).

            - (Optional) Set up a [shared organization](/support/managing-support-contacts/#shared-organizations) so that your authorized support contacts can see each others tickets.

        3. Just in case someone is left out, circulate [instructions on proving your support entitlement](/support/managing-support-contacts/#proving-your-support-entitlement) within your company.

        4. Learn about [how to work effectively in support tickets](/support/#working-effectively-in-support-tickets).

        5. Familiarize yourself with our [Statement of Support](/support/statement-of-support) to understand the scope of what to expect in your interactions.

        6. Take a look at [what's included in Priority Support](/support/#priority-support) and the [effect choosing a support region has on tickets](/support/#effect-on-support-hours-if-a-preferred-region-for-support-is-chosen) to understand the time frame in which you can expect a first response.
    - subtitle:
        text: Setting up an account on support.gitlab.com
        id: setting-up-an-account-on-supportgitlabcom
      text: |
        Account setup can happen one of two ways:

        1. Go to support.gitlab.com and submit a new request. An account and password will be created for you. You will need to request a password reset and setup a new password before you can sign in.
        2. Click on Sign Up to create a new account using your company email address.

        You can keep track of all of your tickets and their current status using the GitLab Support Portal! We recommend using the Support Portal for a superior experience managing your tickets. To learn more about using the Support Portal, watch [this video on using **Zendesk as an end user**](https://www.youtube.com/watch?v=NQzkGD7nIqQ).
    - header: Having trouble with the Support Portal?
      id: having-trouble-with-the-support-portal
      text: |
        Occasionally, you may find the [Support Portal](https://support.gitlab.com/) not acting as expected. This is often caused by the user’s setup. When encountering this, the recommended course of action is:

        1. Ensure your browser is allowing third party cookies. These are often vital for the system to work. A general list to allow would be:
            - `[*.]zendesk.com`
            - `[*.]zdassets.com`
            - `[*.]gitlab.com`
          
        2. Disable all plugins/extensions/addons on the browser.
          
        3. Disable any themes on the browser.
                    
        4. Clear all cookies and cache on the browser.
                   
        5. Try logging in again to the the Support Portal.
                    
        6. If you are still having issues, write down the browser’s version, type, distro, and other identifying information
                    
        7.  Generate a HAR file (process will vary from browser to browser) and send this to support. If you are unable to create a ticket, then communicate with your Technical Account Manager and/or Account Manager. The next best place to send HAR file and browser information is via a GitLab.com issue.
          
    - subtitle:
        text: Language Support
        id: language-support
      text: |
        Ticket support is available in the following languages:
        
        - Chinese
        - English
        - French
        - German
        - Japanese
        - Korean
        - Portuguese
        - Spanish

        While we do not currently provide translated interfaces for our [Support Portal](https://support.gitlab.com/), you can write in your preferred language and we will respond accordingly.
        Should you be offered a call, only English is available.
        **NOTE:** Any attached media used for ticket resolution must be sent in English.
        